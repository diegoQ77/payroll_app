class PayrollSystem:
    # interface acces ponint NOT OOP interface
    def calculate_payroll(self, employees):

        if employees is None:
            return

        if len(employees) == 0:
            return []

        # if employees is None: return None
        # return None if employees is None else [] if len(employees) == 0 else None

        result = [(employee, employee.calculate_payroll())
                  for employee in employees]

        [print(f"Payroll for: {e}\nCheck amount: {p}")
         for e, p in result]

        return result

        # for employee in employees:
        #     print(f'Payroll for: {employee}')
        #     print(f'Check amount: {employee.calculate_payroll()}')
