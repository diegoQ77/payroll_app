# Payroll System

## Instructions

- Add at least 2 unit tests per public methods you find in each class, positive and negative scenario
- app.py: Add at last 2 unit test per public methods you find in each class, positive and negative scenario
- app_v2.py: Make sure this runs successfully

## Run Tests

1; Install dependencies

```python
cd PATH_TO_PROJECT_FOLDER
pip install -r requirements.txt
```

2; Run tests

```python
cd PATH_TO_PROJECT_TESTS_FOLDER
pytest
```
