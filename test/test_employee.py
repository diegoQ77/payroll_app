import pytest
import payroll as hr
from employee import Employee
from employee import SalesAssociate, AdmistrativeWorker, ManufacturingWorker


def test_employee_expects_typeerror():
    match = "Can't instantiate abstract class Employee with abstract method calculate_payroll"
    with pytest.raises(expected_exception=TypeError, match=match):
        Employee(1, "Name")

# test for AdmistrativeWorker class


def test_AdmistrativeWorker():
    salary_employee = AdmistrativeWorker(1, 'John Doe', 1500)
    assert salary_employee is not None
    assert salary_employee.__str__() == 'id: 1, name: John Doe'
    assert type(salary_employee) is AdmistrativeWorker
    assert salary_employee.calculate_payroll() == 1500

    payroll_system = hr.PayrollSystem()
    result = payroll_system.calculate_payroll([
        salary_employee,
    ])
    assert type(result[0][0]) is AdmistrativeWorker
    assert result[0][0] is not None, 'The value not be none'
    assert result[0][1] == 1500, 'The salary must be 1500'


# test for SalesAssociate class


def test_SalesAssociate():
    salary_employee = SalesAssociate(3, 'Foo Fighter', 1000, 250)

    assert salary_employee is not None
    assert salary_employee.__str__() == 'id: 3, name: Foo Fighter'
    assert type(salary_employee) is SalesAssociate
    assert salary_employee.calculate_payroll() == 1250

    payroll_system = hr.PayrollSystem()
    result = payroll_system.calculate_payroll([
        salary_employee,
    ])

    assert type(result[0][0]) is SalesAssociate
    assert result[0][0] is not None, 'The value not be none'
    assert result[0][1] == 1250, 'The salary must be 1250'


# test for SalesAssociate class


def test_ManufactoringWorker():
    salary_employee = ManufacturingWorker(2, 'Jane Doe', 40, 15)

    assert salary_employee is not None
    assert salary_employee.__str__() == 'id: 2, name: Jane Doe'
    assert type(salary_employee) is ManufacturingWorker
    assert salary_employee.calculate_payroll() == 600

    payroll_system = hr.PayrollSystem()
    result = payroll_system.calculate_payroll([
        salary_employee
    ])

    assert type(salary_employee) is ManufacturingWorker
    assert type(result[0][0]) is ManufacturingWorker
    assert result[0][0] is not None, 'The value not be none'
    assert result[0][1] == 600
