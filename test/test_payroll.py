import payroll as hr
from employee import SalesAssociate, AdmistrativeWorker, ManufacturingWorker


def test_calculate_payroll_none_list():
    payroll_system = hr.PayrollSystem()
    array_employees = None
    payroll = payroll_system.calculate_payroll(array_employees)
    assert payroll is None, 'The value must be None'


def test_calculate_payroll_emptylist():
    payroll_system = hr.PayrollSystem()
    payroll = payroll_system.calculate_payroll([])
    assert payroll == [], 'The value must be a empty list'


def test_calculate_payroll_non_emptylist():
    salary_employee = AdmistrativeWorker(1, 'John Doe', 1500)
    payroll_system = hr.PayrollSystem()
    result = payroll_system.calculate_payroll([
        salary_employee,
    ])

    assert result[0][1] == 1500, 'The assert mus be 1500'
    assert result[0][0] == salary_employee, 'Result is not a Administrative worker type'
