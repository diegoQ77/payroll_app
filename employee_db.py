from employee import Employee, AdmistrativeWorker, SalesAssociate, ManufacturingWorker


class EmployeeDatabase:
    def __init__(self):
        self.__employees = [
            {
                'id': 1,
                'name': 'John Doe',
                'role': 'manager',  # is a Admistrative Worker
                'weekly_salary': 1500
            },
            {
                'id': 2,
                'name': 'Jane Doe',
                'role': 'factory',  # is a Manufacturing Worker
                'worked_hours': 40,
                'hour_rate': 15
            },
            {
                'id': 3,
                'name': 'Foo Fighter',
                'role': 'sales',  # is a Sales Associate
                'fixed_salary': 1000,
                'comission': 250
            }
        ]

    def get_employees(self):
        """Returns a list of objects whose parent is class Employee"""
        return [self.__create_employee(employee) for employee in self.__employees]

    # No utilizar [] en diccionarios usar .get('key')
    # Utilizar COnstantes para role, id ,role..
    def __create_employee(self, raw_data):
        """Returns an object which parent is class Employee"""
        if raw_data['role'] == 'manager':
            return AdmistrativeWorker(raw_data['id'], raw_data['name'], raw_data['weekly_salary'])
        if raw_data['role'] == 'factory':
            return ManufacturingWorker(raw_data['id'], raw_data['name'], raw_data['worked_hours'], raw_data['hour_rate'])
        if raw_data['role'] == 'sales':
            return SalesAssociate(raw_data['id'], raw_data['name'], raw_data['fixed_salary'], raw_data['comission'])
        return
